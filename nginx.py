#%%
import pandas as pd
import re
from operator import itemgetter
#%%
def process_log(log):
    requests = get_requests(log)
    return requests
#%%
def get_requests(f):
    log_line = f.read()
    pat = (r''
           '(\d+.\d+.\d+.\d+)\s-\s-\s' #IP address
           '\[(.+)\]\s' #datetime
           '"GET\s(.+)\s\w+/.+"\s' #requested file
           '(\d+)\s' #status
           '(\d+)\s' #bandwidth
           '"(.+)"\s' #referrer
           '"(.+)"' #user agent
        )
    requests = find(pat, log_line)
    return requests
#%%
def find(pat, text):
    match = re.findall(pat, text)
    if match:
        return match
    return False
#%%


log_file = open('2019-09-29-06:00:01.log', 'r')

requests= process_log(log_file)

requests=pd.DataFrame(requests, columns=['IP address', 'datetime', 'requested file','status','bandwidth','referrer','user agent'])
#%%
requests=requests.loc[requests['referrer']=='-']
# %%
import matplotlib.pyplot as plt 
from datetime import datetime
data=[]
for i in requests.datetime:
    data.append(datetime.strptime(i,'%d/%b/%Y:%H:%M:%S %z'))    
# %%
requests['datetime']=data
# %%
requests.plot.line(x='datetime',y='status')
plt.show()

# %%
