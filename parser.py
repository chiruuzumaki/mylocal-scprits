'''
Created on 02-Jul-2018

@author: system
'''
import re
import pandas
message_arrival=re.compile('<=')
message_fakereject=re.compile('\(=')
normal_message_delivery=re.compile('=>')
additional_address_in_same_delivery=re.compile('->')
cutthrough_message_delivery=re.compile('>>')
delivery_suppressed_by_N=re.compile('\*>' )
delivery_failed_address_bounced=re.compile('\*\*')
delivery_deferred_temporary_problem=re.compile('==')
subject_ex=re.compile('T=".*"')
error=re.compile('<>')
reference=re.compile('R=.*')
file=open('exim_mainlog','r')
lines=file.readlines()
toemail=""
fromemail=""
status=''
IPformat=re.compile('\[[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\]')
for temp in lines:
    if message_arrival.search(temp):
        if(error.search(temp)):
          if reference.search(temp):
                ref=reference.findall(temp)[0]
                ref=ref.split("=")[1]
                tomail="NULL"    
        else:
            toemail=temp.split(' ')[4]
            ref='NULL' 
        message_id=temp.split(' ')[2]
        if subject_ex.search(temp):
            subject=subject_ex.findall(temp)[0]
            subject=subject.split("=")[1][1:-1]    
        fromemail=temp.split(' ')[-1]
        status="TO_ourmail"
        date=temp.split(' ')[0]
        time=temp.split(' ')[1]
    
    elif delivery_failed_address_bounced.search(temp):
         toemail=temp.split(' ')[4]
         date=temp.split(' ')[0]
         time=temp.split(' ')[1]
         if re.compile("\/\/").search(temp.split(' ')[-3]) or re.compile("\/\/").search(temp.split(' ')[-1]) or re.compile("\/\/").search(temp.split(' ')[-2]):
             status="Blocked"
         else:
          status=temp.split(' ')[-3]+' '+temp.split(' ')[-2]+" "+temp.split(' ')[-1]
         if re.compile("T=.*").search(temp):
            subject=re.compile("T=[^ ]*").findall(temp)[0].split("=")[1]
    elif normal_message_delivery.search(temp):
        toemail=temp.split("=>")[1]
        print(toemail.split(" ")[1])
          